---
layout: page
title: Full Exit Relay Setup
redirect_from: /wiki/setup/server/
permalink: /exit-relay-setup/
---

## ⚠️ This page is outdated, we are working on a new version ⚠️


<details markdown="block">
  <summary>
    Table of contents
  </summary>
* TOC
{:toc}
</details>

## Basic Setup

  -  We currently use Debian; Ubuntu LTS is another good valid option.
  -  Git: we keep some scripts, configuration files and templates in a git repository. We clone the git repository to the local system and use symlinks when possible. For easier re-use in parts, the example documentation below uses direct links to the configuration files at Github.
 
### SSH key authentication only

  - We use <a href="http://g10code.com/p-card.html" class="urlextern" title="http://g10code.com/p-card.html" rel="ugc nofollow">g10code GnuPG Smartcards</a> for ssh auth; see some notes at <a href="https://github.com/moba/installnotes/blob/master/client/gnupg.md" class="urlextern" title="https://github.com/moba/installnotes/blob/master/client/gnupg.md" rel="ugc nofollow">moba installnotes</a> or more verbose writeup by <a href="https://github.com/flamsmark/documentation/blob/master/gpg/smartcard-keygen.md" class="urlextern" title="https://github.com/flamsmark/documentation/blob/master/gpg/smartcard-keygen.md" rel="ugc nofollow">flamsmark</a>
  - ssh config with password auth and PAM disabled, no root login, x11forwarding disabled  - change port to some port of your chosing above 1024 (when using multiple IPs: bind to one IP only)

<pre class="code">ssh-copy-id -p $SSH_PORT user@server</pre>


### IPtables firewall

This example config uses the iptables-persistent package, and by default allows world access to a random SSH port 23942 and ports 80,443 which we use for Tor. Think about limiting SSH to an IP (range) you can connect from, or use <a href="http://www.debian-administration.org/articles/268" class="urlextern" title="http://www.debian-administration.org/articles/268" rel="ugc nofollow">portknocking</a>. Also, this config is optimized for high bandwidth relays: in order to avoid the conntrack module, it allows all UDP in.

<pre class="code">sudo apt install iptables-persistent
cd /etc/iptables
wget -O iptables.v4 https://raw.githubusercontent.com/torservers/server-config-templates/master/iptables.test.rules
# you need to at least customize SSH port in it now or you will lock yourself out...
# for ipv6, add rules in /etc/iptables/iptables.v6
service iptables-persistent start</pre>

### Some useful defaults

<pre class="code"># configure hostname
hostname yourservername.xyz
vi /etc/hostname # also use yourservername.xyz
vi /etc/hosts # update to yourservername.xyz yourservername

# disable debian default that pulls in recommended packages:
cd /etc/apt/apt.conf.d
wget https://raw.githubusercontent.com/torservers/server-config-templates/master/06norecommends

apt update &amp;&amp; apt full-upgrade
apt install sudo git less htop nload screen \
ntp apticron vnstat logcheck logcheck-database lsb-release
apt remove --purge portmap

sed -i -e 's/^# DIFF_ONLY/DIFF_ONLY/' /etc/apticron/apticron.conf # make apticron send diffs only
vnstat -u -i eth0 # setup vnstat for correct interface
cd /etc
mv aliases aliases.dist
wget https://raw.githubusercontent.com/torservers/server-config-templates/master/aliases
sed -i 's/your@email.address/actual@email.address/' /etc/aliases
newaliases</pre>

### unattended upgrades

We upgrade from all available package sources, let it reboot if necessary, and send mail on errors. A reasonable configuration could be to limit upgrades to the security sources. (see comments in 50unattended-upgrades)

<pre class="code">apt install unattended-upgrades update-notifier-common
wget -p -O /etc/apt/apt.conf.d/50unattended-upgrades https://raw.githubusercontent.com/torservers/server-config-templates/master/50unattended-upgrades
cp /usr/share/unattended-upgrades/20auto-upgrades /etc/apt/apt.conf.d/20auto-upgrades # enable</pre>


## Tor

  - <a href="https://trac.torproject.org/projects/tor/wiki/doc/TorExitGuidelines" class="urlextern" title="https://trac.torproject.org/projects/tor/wiki/doc/TorExitGuidelines" rel="ugc nofollow">https://trac.torproject.org/projects/tor/wiki/doc/TorExitGuidelines</a>
  - anonymizing relay monitor (arm) <a href="http://www.atagar.com/arm/" class="urlextern" title="http://www.atagar.com/arm/" rel="ugc nofollow">http://www.atagar.com/arm/</a> for direct Tor node observation
  - <a href="http://humdi.net/vnstat/" class="urlextern" title="http://humdi.net/vnstat/" rel="ugc nofollow">vnstat</a> for bandwidth stats
  - munin for monitoring

## Preparation & basic setup

See  [Installation of correct packages for Debian/Ubuntu](http://rzuwtpc4wb3xdzrj3yeajsvm3fkq4vbeubm2tdxaqruzzzgs5dwemlad.onion/relay-operators/#operators-4)

for multi-process installations, use tor-instance-create

### Which Ports Should I Use?

<a href="/wiki/faq" class="wikilink1" title="faq" data-wiki-id="faq">FAQ</a>

## Webserver

We not using a standalone webserver on our relay servers. 

## Generate Bandwidth Stats with vnstat(i)

You can generate local graphs and render them as images with vnstati. You could serve these images with a webserver. We don't use this any longer.

<pre class="code">apt install vnstati
# create empty files in root-owned /var/www and change owner to www-data
cd /var/www
touch vnstat.png vnstat_d.png vnstat_m.png vnstat.xml
chown www-data:www-data vnstat*.*
# set up cron job
crontab -u www-data -e</pre>

<pre class="code">*/10 * * * * /usr/bin/vnstati -vs -o /var/www/vnstat.png -i eth0 &gt;/dev/null 2&gt;&amp;1 ;fi
*/10 * * * * /usr/bin/vnstati -d -o /var/www/vnstat_d.png -i eth0 &gt;/dev/null 2&gt;&amp;1 ;fi
1 3 * * * /usr/bin/vnstati -m -o /var/www/vnstat_m.png -i eth0 &gt;/dev/null 2&gt;&amp;1 ;fi
1 3 * * * /usr/bin/vnstat --xml &gt; /var/www/vnstat.xml 2&gt;/dev/null ;fi</pre>

## Local DNS server
<ul>
<li class="level1"><div class="li"> we use unbound, a local caching <abbr title="Domain Name System">DNS</abbr> server</div>
</li>
<li class="level1"><div class="li"> <a href="https://trac.torproject.org/projects/tor/ticket/18580" class="urlextern" title="https://trac.torproject.org/projects/tor/ticket/18580" rel="ugc nofollow">more and more people report problems with unbound</a>; you might want to use 'named' instead.</div>
</li>
<li class="level1"><div class="li"> for optimizations of unbound, see <a href="http://www.unbound.net/documentation/howto_optimise.html" class="urlextern" title="http://www.unbound.net/documentation/howto_optimise.html" rel="ugc nofollow">http://www.unbound.net/documentation/howto_optimise.html</a>; we don't use any of them at the moment, it just works fine out of the box</div>
</li>
<li class="level1"><div class="li"> many people use Google <abbr title="Domain Name System">DNS</abbr>, and it is indeed among the better choices of free <abbr title="Domain Name System">DNS</abbr>. do not use openDNS or other <abbr title="Domain Name System">DNS</abbr> providers that filter <abbr title="Domain Name System">DNS</abbr> requests. for a discussion, see also <a href="https://lists.torproject.org/pipermail/tor-relays/2015-January/006146.html" class="urlextern" title="https://lists.torproject.org/pipermail/tor-relays/2015-January/006146.html" rel="ugc nofollow">Reminder: exit nodes probably shouldn't be using Google's DNS servers</a></div>
</li>
</ul>
<pre class="code">apt install unbound
vi /etc/resolv.conf # insert top: nameserver 127.0.0.1</pre>

</div>

<h1 class="sectionedit14" id="munin_resource_monitoring">Munin Resource Monitoring</h1>
<div class="level1">

Install munin-node and allow remote access from our webserver that runs munin to gather statistics at 194.160.168.61 ( <a href="https://www.torservers.net/munin/" class="urlextern" title="https://www.torservers.net/munin/" rel="ugc nofollow">https://www.torservers.net/munin/</a> ). munin-node is the “client side component”. You might also be interested in the <a href="/wiki/setup/munin" class="wikilink1" title="setup:munin" data-wiki-id="setup:munin">munin "server side component" configuration</a>.

<pre class="code">apt install -y munin-node
ln -s /usr/share/munin/plugins/netstat /etc/munin/plugins/netstat
rm /etc/munin/plugins/http_loadtime
rm /etc/munin/plugins/ntp_*
rm /etc/munin/plugins/postfix_*
rm /etc/munin/plugins/exim_*
sed "s/allow \\^127\\\.0\\\.0\\\.1\\$/allow ^81\\\.7\\\.13\\\.16$/" -i /etc/munin/munin-node.conf
/etc/init.d/munin-node restart</pre>

</div>

# High Bandwidth Tweaks (&gt;100 mbps?)

You might also be interested in this tor-relay thread regarding high speed relay tweaks:
<a href="https://www.mail-archive.com/or-talk@freehaven.net/msg14159.html" class="urlextern" title="https://www.mail-archive.com/or-talk@freehaven.net/msg14159.html" rel="ugc nofollow">How to Run High Capacity Tor Relays</a>

In general, as with all optimizations: you should only apply those that are necessary for you.

## Multiple Tor Processes

Currently, Tor does not scale on multicore CPUs. If the CPU supports AES-NI crypto extensions (most modern CPUs do), one Tor process is able to handle around 400 Mbps of throughput – without AES-NI, around 100 Mbps. If your connection supports more, you will need to run multiple Tor processes. For this, on current Debian/Ubuntu releases, Tor comes with a helper to manage multiple instances. 

Note that running more than two tor processes per IP address will result in those other nodes not being used on the network. You'll see the following message in your logs:

<pre class="code">[notice] Heartbeat: It seems like we are not in the cached consensus.</pre>

(<a href="http://archives.seul.org/or/talk/Jul-2012/msg00296.html" class="urlextern" title="http://archives.seul.org/or/talk/Jul-2012/msg00296.html" rel="ugc nofollow">background</a>)

## Systemd

<pre class="code">tor-instance-create &lt;name&gt;
# torrc for new instance is in /etc/tor/instances/&lt;name&gt;/torrc
systemctl enable tor@&lt;name&gt; # create symlinks for auto-launch of the new instance
systemctl start tor@&lt;name&gt;
systemctl mask tor@default # disable the default tor service</pre>

Done! On systems without systemd, you need to manually configure DataDirectory and PidFile in the torrc.

## sysctl.conf kernel optimizations

Please only tweak kernel settings when necessary! Don't change all at once, pick the ones that make sense and try them one by one over several weeks. Monitor changes carefully.

<pre class="code">cd /etc
mv sysctl.conf sysctl.conf.dist
wget https://raw.githubusercontent.com/torservers/server-config-templates/master/sysctl.conf
# go through the settings once again! some only useful with large memory and CPU
# better tweaking probably possible; magic involved
sysctl -p</pre>

## vnstat MaxBandwidth

Set MaxBandwidth to line maximum, eg. for GBit:

<pre class="code">sed "s/MaxBandwidth 100/MaxBandwidth 1000/g" -i /etc/vnstat.conf
/etc/init.d/vnstat restart # don't reload; will stop vnstat from updating its db...</pre>

## TXQueueLen

Might be useful in some cases. Only optimize when you need to!

<pre class="code"># remove "exit" from rc.local, then
echo 'ifconfig eth0 txqueuelen 20000' &gt;&gt; /etc/rc.local
# Play with it. For GBit I've found values between 8000 and 16000 to be very useful, but it is hardware dependent</pre>

# AES-NI Crypto Acceleration

Recent [Intel](https://ark.intel.com/content/www/us/en/ark/search/featurefilter.html?productType=873&0_AESTech=True) and  [AMD](https://www.amd.com/en/products/specifications/processors)s support a native AES crypto acceleration extension called AES-NI. It is well worth enabling and will save a lot of CPU cycles.

Some motherboards ship with AES-NI disabled. You can check if it is enabled:

<pre class="code"># cat /proc/cpuinfo | grep aes
flags           : fpu [..] popcnt **aes** xsave [..] vpid</pre>
