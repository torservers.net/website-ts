---
layout: page
title: Tor Bridge Setup
permalink: /wiki/setup/bridge/
---
## ⚠️ This page is outdated ⚠️

A more recent version can be found here:<br>
[https://community.torproject.org/relay/setup/bridge/](https://community.torproject.org/relay/setup/bridge/)

## Archived version

Bridges are used by people behind restrictive firewalls that block access to the Tor network. They are only used as entry node to reach the Tor network, and only pass encrypted traffic between Tor clients and Tor middle nodes. As such, they will not generate abuse complaints or any trouble.

**You can help even if you have only 100kb/s and a dynamic IP.**

This guide will set up a bridge on port 443 and pass its details to Torproject, who will take care of distributing it to those in need. If you want to distribute the bridge IP yourself, uncomment the “PublishServerDescriptor 0” line in torrc before starting Tor. If you want to use a different port, change the “ORPort” line.

## Windows (& Mac/KDE?)

See → [https://www.torproject.org/docs/bridges#RunningABridge](https://www.torproject.org/docs/bridges#RunningABridge)

## Debian

What about other Linux distros? Why don't you provide a distro independent solution?

Other distros should be similar. Please add sections on other distros. There is no independent solution, but most Linux distros work very similar.

### Preparation

The Tor Project maintains a [https://www.torproject.org/docs/debian.html.en](https://www.torproject.org/docs/debian.html.en) page with instructions for Debian</a> and Debian packages. This website also has an interface which outputs the correct line for your <code>sources.list</code> file.


    # add tor sources
    echo 'deb     http://deb.torproject.org/torproject.org jessie main' &gt;&gt; /etc/apt/sources.list.d/torproject.list
    echo 'deb     http://deb.torproject.org/torproject.org tor-experimental-0.2.6.x-wheezy main' &gt;&gt; /etc/apt/sources.list.d/torproject.list
    gpg --keyserver keys.gnupg.net --recv 886DDD89
    gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | apt-key add -
    aptitude update

Besides the normal Tor software it is quite useful to also enable Pluggable Transports. This helps users in heavily censored regions to get access to the Internet.

    # use obfs4proxy
    echo 'deb http://deb.torproject.org/torproject.org obfs4proxy main' &gt;&gt; /etc/apt/sources.list.d/torproject.list</pre>


### Installation

    apt-get install tor tor-geoipdb obfs4proxy
    service tor stop
    cd /etc/tor
    mv torrc torrc.dist
    wget -O torrc https://www.torservers.net/misc/config/torrc-bridge
    service tor start

Make sure that incoming connections to port 443 TCP are allowed in your firewall configuration.

## Amazon Elastic Computing Cloud

Here is a tutorial on <a href="http://drwho.virtadpt.net/archive/2011/03/05/running-a-tor-node-from-amazons-elastic-computing-cloud" class="urlextern" title="http://drwho.virtadpt.net/archive/2011/03/05/running-a-tor-node-from-amazons-elastic-computing-cloud" rel="ugc nofollow">how to set up and run a Tor node</a> from the Amazon EC2.

Keep in mind that this tutorial is written by someone who has throttled the amount of traffic their node will relay to 100 <abbr title="Gigabyte">GB</abbr>/month.  By signing up for the <a href="https://aws.amazon.com/free/" class="urlextern" title="https://aws.amazon.com/free/" rel="ugc nofollow">Free Usage Tier</a> you will be allowed to relay about 30GB of traffic before billing begins (for 12 months). After the free usage tier, EC2 is pretty expensive and their pricing model complicated. In our testing, a fairly idle bridge costed $27 for one month.

The Tor Project recently released <a href="https://cloud.torproject.org/" class="urlextern" title="https://cloud.torproject.org/" rel="ugc nofollow">cloud images</a> that can be launched quickly on Amazon EC2.  

# How do I know if it worked?

Open /var/log/tor/log (Linux). The last lines should be similar to this:

    Mar 01 21:24:31.143 [notice] Bootstrapped 100%: Done.
    Mar 01 21:24:31.143 [notice] Now checking whether ORPort YOURIP:443 is reachable... (this may take up to 20 minutes -- look for log messages indicating success)
    Mar 01 21:25:28.438 [notice] Self-testing indicates your ORPort is reachable from the outside. Excellent.
    Mar 01 21:25:33.150 [notice] Performing bandwidth self-test...done.

# Questions?

Don't be afraid to ask. There are helpful people on irc.oftc.net in the channels #tor and #torservers around the clock.
