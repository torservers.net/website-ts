---
layout: page
title: Torservers FAQ
permalink: /wiki/faq/
---
?????? Why are we doing this? ??????
</p>

<p>
Because anonymity is important for many people. See <a href="http://www.torproject.org/about/torusers.html.en" class="urlextern" title="http://www.torproject.org/about/torusers.html.en" rel="ugc nofollow">who uses Tor</a>.
</p>

<p>
?????? Tor Project Inc. develops Tor. Why another organization? ??????
</p>

<p>
Tor Project Inc. develops Tor, but it doesn't provide Tor nodes. All nodes are contributed by volunteers, and while entry and middle nodes operate with little risk, exits provide publicly facing IP addresses, which can be connected to abuses of the network. Therefore, not many people provide exits, and that limits the available bandwidth of the network. Torservers.net aims to expand the pool of exit nodes by helping people set up their own exits and by taking care of abuse complaints and legal issues. So, to the contrary, we need <em>even more</em> organizations! We're happy to help you set one up in your country.
</p>

<p>
We also provide and distribute private bridge IPs to Tor users where regular Tor access is blocked.
</p>

</div>

<h1 class="sectionedit2" id="tor_faq">Tor FAQ</h1>
<div class="level1">

<p>
Main <abbr title="Frequently Asked Questions">FAQ</abbr>: <a href="https://www.torproject.org/faq.html.en" class="urlextern" title="https://www.torproject.org/faq.html.en" rel="ugc nofollow">https://www.torproject.org/faq.html.en</a>
Main Wiki: <a href="https://trac.torproject.org/projects/tor/wiki" class="urlextern" title="https://trac.torproject.org/projects/tor/wiki" rel="ugc nofollow">https://trac.torproject.org/projects/tor/wiki</a>
</p>

</div>

<h2 class="sectionedit3" id="hidden_services">Hidden Services</h2>
<div class="level2">

<p>
?????? What happens if I use the same key on two or more servers? ??????
</p>

<p>
You get a simple form of failover. See <a href="https://lists.torproject.org/pipermail/tor-relays/2011-April/000736.html" class="urlextern" title="https://lists.torproject.org/pipermail/tor-relays/2011-April/000736.html" rel="ugc nofollow">https://lists.torproject.org/pipermail/tor-relays/2011-April/000736.html</a>
</p>

</div>

<h2 class="sectionedit4" id="relays">Relays</h2>
<div class="level2">

<p>
?????? Which Ports Should I Use? ??????
</p>

<p>
Tor will work with any ORPort and DirPort.
</p>

<p>
Tor ORPort traffic is SSL, so the rationale is that your node will be “most reachable” if you use 443 (commonly used for HTTPS). Nobody knows for sure, but what IS known is that there is also a fair amount of users where 443 is definitely blocked, so you should take a look at other node configurations at Torstatus and decide what port could be useful. 22 is probably good too. Same goes for DirPort: The stuff is HTTP, so port 80 makes much sense. Most relays however use port 80 already, so it also makes perfect sense for you NOT to use port 80, because it might be blocked for some Tor users. We use 443/80 for most of our relays.
</p>

<p>
If you have limited bandwidth resources, it is wise to disable directory mirroring and donate your traffic for relaying only.
</p>

<p>
?????? What are the minimal requirements for a 10mbps relay? ??????
</p>

<p>
A virtual machine with 256MB RAM might be enough, but we experienced some out of memory errors with long running nodes. If you can, upgrade to 512MB. You can also experiment with <a href="http://moblog.wiredwings.com/archives/20100427/Tor-on-Debian,-self-compiled-for-better-Performance.html" class="urlextern" title="http://moblog.wiredwings.com/archives/20100427/Tor-on-Debian,-self-compiled-for-better-Performance.html" rel="ugc nofollow">OpenSSL 1.0 and OpenBSD-Malloc</a>. CPU requirements: Hard to say. If you have some idea, edit this.
</p>

<p>
?????? What are the minimal requirements for a high bandwidth relay? ??????
</p>

<p>
Tor currently does not scale on multi-cores. You can however run multiple Tor processes. We have good experience with running one process per CPU core. One core should be good enough for 100 Mbps, likely more. Also check if your CPU supports AES-NI crypto acceleration. See our setup guide for details.
</p>

<p>
?????? [warn] Your computer is too slow to handle this many circuit creation requests! ??????
</p>

<p>
Try setting NumCPU to the actual number of CPUs you have. Scott suggests setting <code>MaxOnionsPending 250</code> ( <a href="https://lists.torproject.org/pipermail/tor-relays/2012-May/001352.html" class="urlextern" title="https://lists.torproject.org/pipermail/tor-relays/2012-May/001352.html" rel="ugc nofollow">https://lists.torproject.org/pipermail/tor-relays/2012-May/001352.html</a> ).
</p>

<p>
?????? [warn] Failing because we have 15967 connections already. Please raise your ulimit -n. ??????
</p>

<p>
Edit /etc/init.d/tor and raise ulimit. Run ulimit -n 65535.
</p>

<p>
??????  kernel: [93953.089233] TCP: Peer xx.xx.xx.xx:27419/34681 unexpectedly shrunk window 2848685075:2848689620 (repaired) ??????
</p>

<p>
You can safely ignore these messages.
</p>