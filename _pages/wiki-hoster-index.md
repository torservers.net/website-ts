---
layout: page
title: Hosters
permalink: /wiki/hoster/index/
---
## ⚠️ This page is outdated ⚠️

A more recent version can be found here:<br>
[http://xmrhfasfg5suueegrnc4gsgyi2tyclcy5oz7f5drnrodmdtob6t2ioyd.onion/relay/community-resources/good-bad-isps/](http://xmrhfasfg5suueegrnc4gsgyi2tyclcy5oz7f5drnrodmdtob6t2ioyd.onion/relay/community-resources/good-bad-isps/)

## Archived version

Each of these has been contacted for partnership in Tor hosting and been told about abuse problems etc. 
Suggest hosters and I will contact them. 

<strong>If you want to read more about our experience with certain providers, have a look at <a href="/wiki/hoster/experience" class="wikilink1" title="hoster:experience" data-wiki-id="hoster:experience">this page</a>.</strong>

<hr>
<div class="table sectionedit2"><table class="inline">
	<thead>
	<tr class="row0">
		<th class="col0"> ISP </th><th class="col1"> Location </th><th class="col2"> custom whois </th><th class="col3"> offer/orientation </th><th class="col4"> comment </th>
	</tr>
	</thead>
	<tbody><tr class="row1">
		<td class="col0"> <a href="https://www.nforce.com/" class="urlextern" title="https://www.nforce.com/" rel="ugc nofollow">nforce.com</a> </td><td class="col1"> NL </td><td class="col2"> Y </td><td class="col3"> 565,25 Euro: 100 TB outbound, inbound free </td><td class="col4"> Processor(s) Intel 4-core 2.4 <abbr title="Gigahertz">GHz</abbr> (X3430), 8GB RAM </td>
	</tr>
	<tr class="row2">
		<td class="col0"> <a href="http://www.axigy.com/" class="urlextern" title="http://www.axigy.com/" rel="ugc nofollow">axigy.com</a> </td><td class="col1"> US </td><td class="col2"> ? </td><td class="col3"> 199$ Intel Xeon X3470 - 4x 2.93 <abbr title="Gigahertz">GHz</abbr>, 8GB RAM </td><td class="col4"></td>
	</tr>
	<tr class="row3">
		<td class="col0"> <a href="http://www.100tb.com/" class="urlextern" title="http://www.100tb.com/" rel="ugc nofollow">100tb.com</a> </td><td class="col1"> US/UK </td><td class="col2"> US:Y;UK:? </td><td class="col3"> UK:$270: 100TB outbound, inbound free, 20ct/<abbr title="Gigabyte">GB</abbr> extra, Quad Xeon 3220, 8GB RAM, 2x500GB </td><td class="col4 leftalign">  </td>
	</tr>
	<tr class="row4">
		<td class="col0"> <a href="http://www.100tb.eu/" class="urlextern" title="http://www.100tb.eu/" rel="ugc nofollow">100tb.eu</a> </td><td class="col1"> NL </td><td class="col2"> Y </td><td class="col3"> 169€: 50TB in + 50TB out, Dual E2160, 4GB RAM; maybe we need the maxi duo HW for 200Euro </td><td class="col4"> “As long as abuse complaints are solved swiftly this is not an issue. Yes we can swip a range of IPs to you.” <sup><a href="#fn__1" id="fnt__1" class="fn_top">1)</a></sup> </td>
	</tr>
	<tr class="row5">
		<td class="col0"> <a href="http://2host.com/" class="urlextern" title="http://2host.com/" rel="ugc nofollow">2Host.com</a> </td><td class="col1"> US </td><td class="col2"> Y </td><td class="col3"> $150: 100TB out, 100TB in, Quad Q9550, 4GB RAM, 2x500GB </td><td class="col4"> custom whois no problem, abuse should be okay; cheap offers at <a href="http://www.webhostingtalk.com/showthread.php?t=971493" class="urlextern" title="http://www.webhostingtalk.com/showthread.php?t=971493" rel="ugc nofollow">webhostingtalk</a> </td>
	</tr>
	<tr class="row6">
		<td class="col0"> <a href="http://www.2x4.ru/" class="urlextern" title="http://www.2x4.ru/" rel="ugc nofollow">2x4.ru</a> </td><td class="col1"> RU </td><td class="col2"> Y </td><td class="col3"> Contact for pricing </td><td class="col4"> tor ok, does not care what you host, will forward abuse </td>
	</tr>
</tbody></table></div>


<h2 class="sectionedit3" id="no_custom_whois">no custom WHOIS</h2>
<div class="level2">
<div class="table sectionedit4"><table class="inline">
	<thead>
	<tr class="row0">
		<th class="col0"> ISP </th><th class="col1"> Location </th><th class="col2"> custom whois </th><th class="col3"> offer/orientation </th><th class="col4"> comment </th>
	</tr>
	</thead>
	<tbody><tr class="row1">
		<td class="col0"> <a href="http://www.t-n-media.de/" class="urlextern" title="http://www.t-n-media.de/" rel="ugc nofollow">t-n media</a> </td><td class="col1"> RO </td><td class="col2"> N </td><td class="col3"> 250€ 1Gbit dedicated Intel Core i7, 6GB RAM, 2x 1,5TB </td><td class="col4"> tor ok, will forward abuse </td>
	</tr>
	<tr class="row2">
		<td class="col0"> <a href="http://www.net100tb.com" class="urlextern" title="http://www.net100tb.com" rel="ugc nofollow">Net100TB</a> </td><td class="col1"> NL </td><td class="col2"> N </td><td class="col3"> 219€: 100TB in+out, Quad, 8GB RAM </td><td class="col4"> </td>
	</tr>
	<tr class="row3">
		<td class="col0"> <a href="http://www.yazuhost.com/" class="urlextern" title="http://www.yazuhost.com/" rel="ugc nofollow">Yazuhost</a> </td><td class="col1"> NL </td><td class="col2"> N </td><td class="col3"> 150€ 100TB up 100TB down Xeon 3210, 4GB RAM, 500GB </td><td class="col4"> tor ok, Leaseweb reseller, can forward DMCA complaints </td>
	</tr>
	<tr class="row4">
		<td class="col0"> <a href="http://www.zenex5ive.com/" class="urlextern" title="http://www.zenex5ive.com/" rel="ugc nofollow">Xenex</a> </td><td class="col1"> USA </td><td class="col2"> N </td><td class="col3"> $379 E5620 8GB 2x500GB 100TB </td><td class="col4"> “We cannot reassign IP blocks.” </td>
	</tr>
</tbody></table></div>

</div>

<h2 class="sectionedit5" id="currently_contacting">currently contacting</h2>
<div class="level2">
<div class="table sectionedit6"><table class="inline">
	<thead>
	<tr class="row0">
		<th class="col0"> ISP </th><th class="col1"> Location </th><th class="col2"> custom whois </th><th class="col3"> offer/orientation </th><th class="col4"> comment </th>
	</tr>
	</thead>
	<tbody><tr class="row1">
		<td class="col0"> <a href="http://RapidSpeeds.com" class="urlextern" title="http://RapidSpeeds.com" rel="ugc nofollow">RapidSpeeds</a> </td><td class="col1 leftalign">  </td><td class="col2"> </td><td class="col3"> 140GBP 1Gbit unmetered </td><td class="col4"> Not going to host you. no custom WHOIS</td>
	</tr>
	<tr class="row2">
		<td class="col0 leftalign"> <a href="http://thordc.is" class="urlextern" title="http://thordc.is" rel="ugc nofollow">thordc</a>  </td><td class="col1 leftalign">  </td><td class="col2"> </td><td class="col3"> </td><td class="col4 rightalign">  <a href="/wiki/hoster/thordc.is" class="wikilink1" title="hoster:thordc.is" data-wiki-id="hoster:thordc.is">HW prices</a> </td>
	</tr>
	<tr class="row3">
		<td class="col0"> <a href="http://rokabear.com/" class="urlextern" title="http://rokabear.com/" rel="ugc nofollow">Rokabear</a> </td><td class="col1 leftalign">  </td><td class="col2"> </td><td class="col3"> </td><td class="col4"> </td>
	</tr>
</tbody></table></div>

</div>

<h2 class="sectionedit7" id="expensive">expensive</h2>
<div class="level2">
<div class="table sectionedit8"><table class="inline">
	<thead>
	<tr class="row0">
		<th class="col0"> ISP </th><th class="col1"> Location </th><th class="col2"> custom whois </th><th class="col3"> offer/orientation </th><th class="col4"> comment </th>
	</tr>
	</thead>
	<tbody><tr class="row1">
		<td class="col0"> <a href="http://www.leaseweb.com" class="urlextern" title="http://www.leaseweb.com" rel="ugc nofollow">Leaseweb</a> </td><td class="col1"> NL </td><td class="col2"> N </td><td class="col3"> ~106 euro 100mbit/s dedi unshared, E2160, 4GB </td><td class="col4"> always forwards all abuse complaints </td>
	</tr>
	<tr class="row2">
		<td class="col0"> <a href="http://UnmeteredServers.com" class="urlextern" title="http://UnmeteredServers.com" rel="ugc nofollow">UnmeteredServers</a> </td><td class="col1"> ? </td><td class="col2"> N </td><td class="col3"> $749.99/Month $0/Setup Fee , 2nd Month Free, Dual Quad-Core Intel Xeon e5620 2.4Ghz 12GB ECC DDR2/3 Memory </td><td class="col4"> “should abuse complaints get to overwhelming we would no choice but to cancel the service as we need to keep our ipspace clean” <sup><a href="#fn__2" id="fnt__2" class="fn_top">2)</a></sup> </td>
	</tr>
	<tr class="row3">
		<td class="col0"> <a href="http://www.cinipac.com/" class="urlextern" title="http://www.cinipac.com/" rel="ugc nofollow">Cinipac</a> </td><td class="col1"> eg. IRAN </td><td class="col2"> ? </td><td class="col3"> 130€ 3000GB </td><td class="col4"> privacy hoster </td>
	</tr>
	<tr class="row4">
		<td class="col0"> <a href="http://www.prq.se" class="urlextern" title="http://www.prq.se" rel="ugc nofollow">PRQ</a></td><td class="col1"> SE </td><td class="col2"> Y </td><td class="col3"> too expensive </td><td class="col4"> ex the piratebay hoster </td>
	</tr>
	<tr class="row5">
		<td class="col0"> <a href="http://www.rapidswitch.com/DedicatedServers.aspx" class="urlextern" title="http://www.rapidswitch.com/DedicatedServers.aspx" rel="ugc nofollow">Rapidswitch</a> </td><td class="col1"> UK </td><td class="col2"> N </td><td class="col3"> 100€ 10TB </td><td class="col4"> As long as the use of the server abides by these guidelines it is ok. Please note that if we do receive an abuse report for the server we can suspend it. </td>
	</tr>
	<tr class="row6">
		<td class="col0"> <a href="http://www.santrex.net/" class="urlextern" title="http://www.santrex.net/" rel="ugc nofollow">Santrex</a> </td><td class="col1"> FR/US/DE/UK </td><td class="col2"> Y </td><td class="col3"> $160 10TB </td><td class="col4"> 24h-48h response time </td>
	</tr>
	<tr class="row7">
		<td class="col0"> <a href="http://www.sh3lls.net/dedicated.htm" class="urlextern" title="http://www.sh3lls.net/dedicated.htm" rel="ugc nofollow">Sh3lls</a> </td><td class="col1"> US </td><td class="col2"> Y </td><td class="col3"> $90 10mbit, $300 100mbit </td><td class="col4"> “timely” reaction; also see <a href="https://secure.sh3lls.net/viewticket.php?tid=318437&amp;c=vGvu5uwz" class="urlextern" title="https://secure.sh3lls.net/viewticket.php?tid=318437&amp;c=vGvu5uwz" rel="ugc nofollow">https://secure.sh3lls.net/viewticket.php?tid=318437&amp;c=vGvu5uwz</a> </td>
	</tr>
	<tr class="row8">
		<td class="col0"> <a href="http://www.netrouting.nl" class="urlextern" title="http://www.netrouting.nl" rel="ugc nofollow">Netrouting</a> </td><td class="col1"> NL </td><td class="col2"> N </td><td class="col3"> €50 10mbit E2220, 2GB </td><td class="col4"> “timely” reaction </td>
	</tr>
	<tr class="row9">
		<td class="col0"> <a href="http://serverconnect.se/" class="urlextern" title="http://serverconnect.se/" rel="ugc nofollow">Serverconnect</a> </td><td class="col1"> NL </td><td class="col2"> ? </td><td class="col3"> 220€ 10TB </td><td class="col4"> ? </td>
	</tr>
	<tr class="row10">
		<td class="col0"> <a href="http://www.shinjiru.com/" class="urlextern" title="http://www.shinjiru.com/" rel="ugc nofollow">Shinjiru</a> </td><td class="col1"> MY </td><td class="col2"> Y </td><td class="col3"> $190 1500GB </td><td class="col4"> tor sponsor, privacy hoster malaysia </td>
	</tr>
	<tr class="row11">
		<td class="col0 leftalign"> <a href="http://directspace.net" class="urlextern" title="http://directspace.net" rel="ugc nofollow">DirectSpace</a>  </td><td class="col1"> US </td><td class="col2"> ? </td><td class="col3"> Xeon 5110 , 4GB RAM, 2x500GB, 100TB up 100TB down, $10/TB </td><td class="col4"> custom notice should be no problem, very poor communication. no reply about custom whois. </td>
	</tr>
	<tr class="row12">
		<td class="col0 leftalign"> <a href="http://www.caratnetworks.com" class="urlextern" title="http://www.caratnetworks.com" rel="ugc nofollow">Carat</a>  </td><td class="col1"> CA </td><td class="col2"> ? </td><td class="col3"> 300€ 10TB </td><td class="col4"> known privacy hoster </td>
	</tr>
</tbody></table></div>

</div>

<h2 class="sectionedit9" id="avoid_these_isps">avoid these ISPs</h2>
<div class="level2">
<div class="table sectionedit10"><table class="inline">
	<thead>
	<tr class="row0">
		<th class="col0"> ISP </th><th class="col1"> reference </th>
	</tr>
	</thead>
	<tbody><tr class="row1">
		<td class="col0"> <a href="http://fdcservers.net" class="urlextern" title="http://fdcservers.net" rel="ugc nofollow">FDCservers</a> </td><td class="col1"> <a href="http://www.freelists.org/post/torservers/FDC-cancelled-our-server-FDC-history" class="urlextern" title="http://www.freelists.org/post/torservers/FDC-cancelled-our-server-FDC-history" rel="ugc nofollow">kicked us within a month</a> </td>
	</tr>
	<tr class="row2">
		<td class="col0"> <a href="http://ecatel.net" class="urlextern" title="http://ecatel.net" rel="ugc nofollow">Ecatel</a> </td><td class="col1"> bad ISP, ignores all abuse, bad reputation </td>
	</tr>
	<tr class="row3">
		<td class="col0"> <a href="http://evoboxes.net" class="urlextern" title="http://evoboxes.net" rel="ugc nofollow">EvoBoxes</a> </td><td class="col1"> <a href="http://www.freelists.org/post/torservers/evoboxes-node,1" class="urlextern" title="http://www.freelists.org/post/torservers/evoboxes-node,1" rel="ugc nofollow">http://www.freelists.org/post/torservers/evoboxes-node,1</a> </td>
	</tr>
	<tr class="row4">
		<td class="col0"> <a href="http://www.Incero.com" class="urlextern" title="http://www.Incero.com" rel="ugc nofollow">Incero</a> </td><td class="col1"> <a href="http://www.freelists.org/post/torservers/Fwd-Re-Tor-Server-w-ARIN-reassignment" class="urlextern" title="http://www.freelists.org/post/torservers/Fwd-Re-Tor-Server-w-ARIN-reassignment" rel="ugc nofollow">"We don't allow TOR"</a> </td>
	</tr>
	<tr class="row5">
		<td class="col0"> MakoSolutions </td><td class="col1"> “Open proxies are forbidden.” </td>
	</tr>
	<tr class="row6">
		<td class="col0"> <a href="http://www.redstation.com" class="urlextern" title="http://www.redstation.com" rel="ugc nofollow">Redstation</a> </td><td class="col1"> against AUP term 3 “tunneling unidentified traffic” </td>
	</tr>
	<tr class="row7">
		<td class="col0"> <a href="http://Securedservers.com" class="urlextern" title="http://Securedservers.com" rel="ugc nofollow">Securedservers</a> </td><td class="col1"> likes Tor, but carrier does not </td>
	</tr>
</tbody></table></div>

</div>

<h1 class="sectionedit11" id="provider_independent_ip_space">Provider Independent IP Space</h1>
<div class="level1">
<ul>
<li class="level1"><div class="li"> <a href="https://en.wikipedia.org/wiki/Provider-independent_address_space" class="interwiki iw_wp" title="https://en.wikipedia.org/wiki/Provider-independent_address_space">Provider-independent_address_space</a></div>
</li>
<li class="level1"><div class="li"> would be excellent to have</div>
</li>
<li class="level1 node"><div class="li"> expensive</div>
<ul>
<li class="level2"><div class="li"> <a href="http://lir.at" class="urlextern" title="http://lir.at" rel="ugc nofollow">http://lir.at</a> : 1000 EUR setup, 100 EUR yearly</div>
</li>
<li class="level2"><div class="li"> <a href="http://openpeering.nl" class="urlextern" title="http://openpeering.nl" rel="ugc nofollow">http://openpeering.nl</a> : 650 EUR setup, 150 EUR yearly</div>
</li>
<li class="level2"><div class="li"> <a href="http://CB3ROB.NET" class="urlextern" title="http://CB3ROB.NET" rel="ugc nofollow">http://CB3ROB.NET</a> : even more expensive </div>
</li>
<li class="level2"><div class="li"> smaller nets than 256 are not routed, you have to at least use 25% of all IPs, and they all have to be routed to the same ISP → use IPs for bridges </div>
</li>
<li class="level2"><div class="li"> Yes the old justification used to be for SSL'ed hosting, I think we can easily justify this, as tor is SSL, and if we have several bridges…</div>
</li>
<li class="level2"><div class="li"> RIPE NCC charge 50€ for PI/AS issued on the same day see <a href="http://www.ripe.net/ripe/docs/ripe-499" class="urlextern" title="http://www.ripe.net/ripe/docs/ripe-499" rel="ugc nofollow">http://www.ripe.net/ripe/docs/ripe-499</a> We just need to find a member who doesn't want funny numbers as above.</div>
